import sys
import vobject
import copy
import hashlib

def merge(c1, c2):
    merged = []
    contacts = c1 + c2

    for c in contacts:
        for x in merged:
            if contact_is_same(c, x):
                break
        else:
            merged.append(copy.deepcopy(c))

    return merged

def contact_is_same(c1, c2):
    if c1.fn.value != "" and c1.fn.value == c2.fn.value:
        return True

    try:
        if c1.email.value != "" and c1.email.value == c2.email.value:
            return True
    except (KeyError, AttributeError) as e:
        pass

    try:
        if c1.tel.value != "" and c1.tel.value == c2.tel.value:
            return True
    except (KeyError, AttributeError) as e:
        pass

    return False

def print_contact(c):
    try:
        email = c.email.value
    except (KeyError, AttributeError):
        email = ""

    try:
        tel = c.tel.value
    except (KeyError, AttributeError):
        tel = ""
        
    print("%s :: %s :: %s" % (c.fn.value, email, tel))

if __name__ == "__main__":
    with open(sys.argv[1], "r", encoding="utf-8") as f:
        contacts1 = list(vobject.readComponents(f.read()))

    with open(sys.argv[2], "r", encoding="utf-8") as f:
        contacts2 = list(vobject.readComponents(f.read()))

    with open(sys.argv[3], "w", encoding="utf-8") as f:
        for c in merge(contacts1, contacts2):
            f.write(c.serialize().replace("\r\n", "\n"))
