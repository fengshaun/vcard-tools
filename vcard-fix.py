import sys
import vobject
import copy

def unique_contacts_in_first(contacts1, contacts2):
    # in c1 but not in c2
    in_c1 = []
    for c1 in contacts1:
        for c2 in contacts2:
            if contact_is_same(c1, c2):
                break
        else:
            in_c1.append(copy.deepcopy(c1))

    return in_c1

def print_contact(c):
    try:
        email = c.email.value
    except (KeyError, AttributeError):
        email = ""

    try:
        tel = c.tel.value
    except (KeyError, AttributeError):
        tel = ""
        
    print("%s :: %s :: %s" % (c.fn.value, email, tel))

if __name__ == "__main__":
    with open(sys.argv[1], "r", encoding="utf-8") as f:
        contacts1 = list(vobject.readComponents(f.read()))

    fixed = []
    for c in contacts1:
        print_contact(c)
        if c.fn.value == "":
            i = input("FN (or del to delete): ")
            if i == "del":
                continue

            c.fn.value = i

        fixed.append(copy.deepcopy(c))

    with open(sys.argv[2], "w", encoding="utf-8") as f:
        for c in fixed:
            f.write(c.serialize())
