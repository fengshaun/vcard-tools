import sys
import vobject
import copy

def unique_contacts_in_first(contacts1, contacts2):
    # in c1 but not in c2
    in_c1 = []
    for c1 in contacts1:
        for c2 in contacts2:
            if contact_is_same(c1, c2):
                break
        else:
            in_c1.append(copy.deepcopy(c1))

    return in_c1

def contact_is_same(c1, c2):
    if c1.fn.value != "" and c1.fn.value == c2.fn.value:
        return True

    try:
        if c1.email.value != "" and c1.email.value == c2.email.value:
            return True
    except (KeyError, AttributeError) as e:
        pass

    try:
        if c1.tel.value != "" and c1.tel.value == c2.tel.value:
            return True
    except (KeyError, AttributeError) as e:
        pass

    return False

def print_report(name, l):
    print("In %s only: %d" % (name, len(l)))
    for c in l:
        print_contact(c)

def print_contact(c):
    try:
        email = c.email.value
    except (KeyError, AttributeError):
        email = ""

    try:
        tel = c.tel.value
    except (KeyError, AttributeError):
        tel = ""
        
    print("%s :: %s :: %s" % (c.fn.value, email, tel))

if __name__ == "__main__":
    with open(sys.argv[1], "r", encoding="utf-8") as f:
        contacts1 = list(vobject.readComponents(f.read()))

    with open(sys.argv[2], "r", encoding="utf-8") as f:
        contacts2 = list(vobject.readComponents(f.read()))

    print_report(sys.argv[1], unique_contacts_in_first(contacts1, contacts2))
    print_report(sys.argv[2], unique_contacts_in_first(contacts2, contacts1))
